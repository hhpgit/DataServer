package com.imarad.server.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Copyright2017-2017  AMBITION.All Rights Reserved
 *
 * Comments：数据转换类
 *
 * @author wangj
 *
 *         Time: 2017/11/20
 *
 *         Modified By:
 *         Modified Date:
 *         Why & What is modified:
 * @version 1.0.0
 */
public class DataConversion {
    private static final String REX_NET_TYPE = "localhost|127.0.0.1|"
                                               + "((192\\.168|172\\.([1][6-9]|[2]\\d|3[01]))"
                                               + "(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){2}|"
                                               + "^(\\D)*10(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){3})";

    public static void main(String[] args) {
        isInternet("127.10.25.32");
    }

    public static boolean isInternet(String ip) {
        return !matchIgnoreCase(ip, REX_NET_TYPE);
    }

    private static boolean match(String srcStr, String regEx, Pattern pattern) {
        Matcher matcher = pattern.matcher(srcStr);
        // 字符串是否与正则表达式相匹配
        boolean isMatch = matcher.matches();
        System.out
            .println("ClientUtils:>>>匹配字段：" + srcStr + ", 匹配规则：" + regEx + ", 是否匹配：" + isMatch);
        return isMatch;
    }

    /**
     * 正则匹配 (不区分大小写)
     *
     * @param srcStr 匹配字符串
     * @param regEx  正则规则
     * @return 是否匹配规则
     */
    public static boolean matchIgnoreCase(String srcStr, String regEx) {
        Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
        return match(srcStr, regEx, pattern);
    }

}

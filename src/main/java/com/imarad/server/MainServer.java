package com.imarad.server;

import org.apache.commons.lang3.StringUtils;

/**
 * nio server 启动类
 * 可以通过参数-Dserver.socket.port=9090改变端口
 * 默认端口8092
 * <p />
 * 可以通过参数-Dorg.slf4j.simpleLogger.defaultLogLevel=debug
 * 更改日志等级默认为info
 * @author hhu 【huan.hu@cnambition.com】
 * @version com.imarad.demo, v 0.1 2018/1/29 15:23 hhu Exp $$
 */
public class MainServer {
    private static final String PORT_KEY = "server.socket.port";

    static {
        if (StringUtils.isEmpty(System.getProperty(PORT_KEY))) {
            System.setProperty(PORT_KEY, "8092");
        }
    }

    public static void main(String[] args) {
        //        int port = Integer.parseInt(System.getProperty(PORT_KEY));
        //        ExecutorService pool = Executors.newSingleThreadExecutor();
        //        pool.execute(new BioServer(port));

        double dm = Math.ceil((double)10000 / 3);
        System.out.println((double) 10000 / 3);
    }
}
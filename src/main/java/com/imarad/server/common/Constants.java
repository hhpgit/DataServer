package com.imarad.server.common;

/**
 * @author hhu 【huan.hu@cnambition.com】
 * @version com.imarad.demo, v 0.1 2018/1/30 14:13 hhu Exp $$
 */
public class Constants {
    /**
     * 接收数据包最大
     */
    public static final int  RECEVIE_SOCKET_PACKET_TOTAL_LEN = 1500;
    /**
     * 接入指令
     */
    public static final byte LOGIN_COMMAND                   = 1;
    /**
     * 信号量
     */
    public static final byte SEMAPHORE                       = -99;
    /**
     * 数据包包头长度
     */
    public static final int         PACKET_HEADER_LEN               = 17;
    /**
     * 发送数据指令
     */
    public static final byte SEND_MSG_COMMAND = 3;
}

package com.imarad.server.common;

import java.util.Arrays;

import com.imarad.server.util.ServerUtils;

/**
 * @author hhu 【huan.hu@cnambition.com】
 * @version com.imarad.demo, v 0.1 2018/1/30 14:03 hhu Exp $$
 */
public class ClientPacket {
    /**
     * 命令
     */
    private byte   command;
    /**
     * 当前包id
     */
    private int    packetId;
    /**
     * 当前包长
     */
    private int    packetLen;
    /**
     * 所有包总长度
     */
    private int    totalLen;
    /**
     * 总小包数量
     */
    private int    packetCount;

    private byte[] data;

    public ClientPacket() {
    }

    public ClientPacket(byte[] buff) {
        int cursor = 0;
        this.command = buff[cursor++];
        this.packetId = ServerUtils.bytesToInt(buff, cursor);
        cursor += 4;
        this.packetLen = ServerUtils.bytesToInt(buff, cursor);
        cursor += 4;
        this.totalLen = ServerUtils.bytesToInt(buff, cursor);
        cursor += 4;
        this.packetCount = ServerUtils.bytesToInt(buff, cursor);
        cursor += 4;
        if ((packetLen + Constants.PACKET_HEADER_LEN) <= buff.length) {
            data = new byte[packetLen];
            System.arraycopy(buff, cursor, data, 0, packetLen);
        }
    }

    public byte[] getSendData() {
        int cursor = 0;
        byte[] buffer = new byte[Constants.PACKET_HEADER_LEN + this.getPacketLen()];
        buffer[cursor++] = this.getCommand();

        System.arraycopy(ServerUtils.intToBytes(this.getPacketId()), 0, buffer, cursor, 4);
        cursor += 4;

        System.arraycopy(ServerUtils.intToBytes(this.getPacketLen()), 0, buffer, cursor, 4);
        cursor += 4;

        System.arraycopy(ServerUtils.intToBytes(this.getPacketCount()), 0, buffer, cursor, 4);
        cursor += 4;

        System.arraycopy(ServerUtils.intToBytes(this.getTotalLen()), 0, buffer, cursor, 4);
        cursor += 4;

        System.arraycopy(this.getData(), 0, buffer, cursor, this.getPacketLen());

        return buffer;
    }

    public byte getCommand() {
        return command;
    }

    public void setCommand(byte command) {
        this.command = command;
    }

    public int getPacketId() {
        return packetId;
    }

    public void setPacketId(int packetId) {
        this.packetId = packetId;
    }

    public int getPacketLen() {
        return packetLen;
    }

    public void setPacketLen(int packetLen) {
        this.packetLen = packetLen;
    }

    public int getTotalLen() {
        return totalLen;
    }

    public void setTotalLen(int totalLen) {
        this.totalLen = totalLen;
    }

    public int getPacketCount() {
        return packetCount;
    }

    public void setPacketCount(int packetCount) {
        this.packetCount = packetCount;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClientPacket{" + "command=" + command + ", packetId=" + packetId + ", packetLen="
               + packetLen + ", totalLen=" + totalLen + ", packetCount=" + packetCount + ", data="
               + Arrays.toString(data) + '}';
    }
}